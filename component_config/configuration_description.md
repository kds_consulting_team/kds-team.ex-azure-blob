1. Account Name - `Required`
    - Azure storage account name
2. Account Key - `Required`
    - Azure storage account access key
    - Account Key can be found:
      ```
      [Your Storage Account Overview] > Settings > Access Keys
      ```
3. Container Name - `Required`
    - Azure storage container name
4. Files - `Required`
    - Specify what files from Blob storage the user wish to extract
    - File configurations

        1. File Name - `Required`
            - Remote absolute/relative table path
            - Required to enter the name of a source file with its extension. Example: test.csv
            - Wildcard, `*`, is available. It fetches multiple tables that matches with the [GLOB syntax](https://en.wikipedia.org/wiki/Glob_(programming))

                Scenarios

                1. `test_*` will download all CSV files with `test_` in the prefix
                2. `sub_folder/*` will download all CSV files from the `sub_folder` that are not a folder.
                3. Any subfolders matching the GLOB pattern won't be downloaded recursively. With `new_folder/*` configured, the contents in `new_folder/new_sub_folder` will not be downloaded.
            
        2. Storage Name - `Required`
            - Result table name
            - Will be used as the output table name in Keboola Storage
            
        3. Incremental
            - Incrementally load the files into Keboola storage
            - For tables without primary keys, the output table will be append into the exisiting table
            - For tables with primary keys, the output table will be updated if rows exist while the output table will be inserted if rows do not exist.
        4. Primary Key
            - Not Required
            - If primary key is not specified with incremental load enabled, the output table will be append into the existing table in Keboola storage

## Constraints

The component is not configured to loop through the list of containers or the folders available in the account. Each extractor configuration is configured to connect to one Container and each Blob Files' row configuration is configured to extract one folder only.
