# Azure Blob Storage Extractor

The purpose of Azure Blob Storage Extractor is to extract any CSV files from the configured Azure Blob container.

## Token Permission Requirements
    Allowed Services:
        1. Blob
    Allowed resource types:
        1. Service
        2. Container
        3. Object
    Allowed Permissions:
        1. Read
        2. List

## Constraints

The component is not configured to loop through the list of containers or the folders available in the account. Each extractor configuration is configured to connect to one Container and each Blob Files' row configuration is configured to extract one folder only.

## Configurations

1. Account Name - `Required`
    - Azure storage account name
2. Account Key - `Required`
    - Azure storage account access key
    - Account Key can be found:
      ```
      [Your Storage Account Overview] > Settings > Access Keys
      ```
3. Container Name - `Required`
    - Azure storage container name
4. Files - `Required`
    - Specify what files from Blob storage the user wish to extract
    - File configurations

        1. File Name - `Required`
            - Required to enter the name of a source file with its extension. Example: `folder/subfolder/test.csv`
            - Alternatively, users have the option to use wildcard with `*`. The wildcard configuration will only loop through configured folder. Meaning if there is a folder within the `current` folder that fits the wildcard description, the component will *NOT* loop through that folder that matches the wildcard configuration.
                - Example: With `test_*` configured, the component will extract any CSV files that contain `test_` in the prefix. Files like `test_1.csv`, `test_.csv` will be extracted.
                - Example: If user is looping through a `test_*` wildcard and there is a folder called `test_folder`, the component will NOT loop through any content in that folder.
            
        2. Storage Name - `Required`
            - User will need to specify the storage table name. Component will be using this name for the output table in KBC storage.
        3. Incremental
            - User has the option to select whether or not incrementally load the files into KBC
            - If primary key is not specified with incremental load enabled, the output table will be append into the exisiting table in KBC storage
        4. Primary Key
            - Not Required
            - If primary key is not specified with incremental load enabled, the output table will be append into the existing table in KBC storage

